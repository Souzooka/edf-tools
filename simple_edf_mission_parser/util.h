#pragma once

void Read4Bytes( unsigned char *chunk, std::vector<char> buf, int pos );

void Read4BytesReversed( unsigned char *chunk, std::vector<char> buf, int pos );

int GetIntFromChunk( unsigned char *chunk );

std::wstring ToString( int i );
std::wstring ToString( float f );

std::wstring ReadUnicode( std::vector<char> chunk, int pos, bool swapEndian = false );