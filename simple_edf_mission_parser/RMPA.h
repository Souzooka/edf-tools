#pragma once

//RMPA spawnpoint
struct RMPASpawnPoint
{
	RMPASpawnPoint( const std::wstring& nm, float ix, float iy, float iz, float p, float ya, float ro )
	{
		name = nm;
		x = ix;
		y = iy;
		z = iz;

		pitch = p;
		yaw = ya;
		roll = ro;
	}

	std::wstring name;

	float x;
	float y;
	float z;

	float pitch;
	float yaw;
	float roll;
};

//RMPA Enumerator
struct RMPAEnumerator
{

};

class CRMPA
{
public:
	int Read( const std::wstring& path );

	RMPASpawnPoint ReadSpawnpoint( int pos, std::vector<char> buffer );

private:
	std::vector< RMPASpawnPoint > spawnPoints;
};