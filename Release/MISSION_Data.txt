
Voice2( arg0, arg1 )
{
	relIndex -= 3
	game->2C(4000, localVar1, arg1, arg0 )
	game->Function_0xA2(  )
	game->Function_0xc8( localVar2 )
	relIndex += 1
}
RadioBegin(  )
{
	relIndex -= 1
	game->2C(4000, "MusenBegin" )
	relIndex += 1
}
RadioEnd(  )
{
	relIndex -= 1
	game->2C(4000, "MusenEnd" )
	relIndex += 3
}
RadioVoice( arg0, arg1 )
{
	relIndex -= 3
	game->Function_0xA2(  )
	game->Function_0xc8( localVar2 )
	game->2C(4000, localVar1, arg1, arg0 )
	game->Function_0xA2(  )
	relIndex += 2
}
EconomyMode( arg0 )
{
	relIndex -= 2
	game->Function_0x11( localVar1 )
	relIndex += 1
}
InitializeCommon(  )
{
	relIndex -= 1
	game->Function_0x05(  )
	relIndex += 3
}
MissionClear(  )
{
	relIndex -= 3
	game->2C(301, 2.000000 )
	game->Function_0xc8( 1.500000 )
	game->Factor_Wait( false )
	RAS[ relIndex + 1 ] = game->Function_1E( "ui_missionclear" )
	game->PlayBgm( "Jingle_MissionCleared" )
	game->Function_0xc8( 6.000000 )
	RAS[ relIndex + 2 ] = game->Function_1E( "ui_fade_screen_test" )
	game->2C(38, 3.000000, 3, localVar2 )
	game->Function_0x1F( localVar1 )
	game->Function_0x1F( localVar2 )
	game->Function_0x03( true )
	relIndex += 3
}
FinalMissionClear(  )
{
	relIndex -= 3
	game->2C(301, 2.000000 )
	game->Function_0xc8( 1.500000 )
	game->Factor_Wait( false )
	game->Function_0x10(  )
	RAS[ relIndex + 1 ] = game->Function_1E( "ui_missionclear" )
	game->PlayBgm( "Jingle_MissionClearedFinal" )
	game->Function_0xc8( 6.000000 )
	RAS[ relIndex + 2 ] = game->Function_1E( "ui_fade_screen_test" )
	game->2C(38, 3.000000, 3, localVar2 )
	game->Function_0x1F( localVar1 )
	game->Function_0x1F( localVar2 )
	game->Function_0x03( true )
	relIndex += 5
}
MissionGameOverEvent(  )
{
	relIndex -= 5
	game->Function_0xc8( 3.000000 )
	game->Factor_Wait( false )
	game->2C(301, 2.000000, false )
	game->Function_0xc8( 1.500000 )
	RAS[ relIndex + 2 ] = game->Function_1E( "ui_missionfailed" )
	game->PlayBgm( "Jingle_MissionFailed" )
	game->Function_0xc8( 5.000000 )
	game->2C(37, false, " ", " ", relIndex + 3 )
	RAS[ var ] = var
	RAS[ relIndex + 4 ] = game->Function_1E( "ui_fade_screen_test" )
	game->2C(38, 0.500000, 3, localVar4 )
	game->Function_0x1F( localVar2 )
	game->Function_0x1F( localVar4 )
	game->Function_0x03( 3 )
	game->Function_0x03( 2 )
	relIndex += 1
}
Main(  )
{
	relIndex -= 1
	game->2C(18, false, false, false, false )
	EconomyMode( false )
	InitializeCommon(  )
	game->2C(10,  )
	game->2C(14, 255, "fine", "app:/Map/nw_Kasenjiki01.mac" )
	game->LoadResource( 255, "app:/Object/AiArmySoldier_AF_Leader.sgo" )
	game->LoadResource( 255, "app:/Object/AiArmySoldier_AF.sgo" )
	game->LoadResource( 255, "app:/Object/GiantAnt01B.sgo" )
	game->LoadResource( 255, "app:/Object/GiantAnt01.sgo" )
	game->LoadResource( 255, "app:/object/man01.sgo" )
	game->LoadResource( 255, "app:/object/man02.sgo" )
	game->LoadResource( 255, "app:/object/man03.sgo" )
	game->LoadResource( 255, "app:/object/woman01.sgo" )
	game->LoadResource( 255, "app:/object/woman02.sgo" )
	game->LoadResource( 255, "app:/Object/AiPaleWing_Lance_Leader.sgo" )
	game->LoadResource( 255, "app:/Object/AiPaleWing_Lance.sgo" )
	game->Function_0x10_00(  )
	game->CheckResourcesLoaded(  )
	game->Function_0x10(  )
	game->Function_0x0B(  )
	game->SetMap( "fine", "app:/Map/nw_Kasenjiki01.mac" )
	game->CreatePlayer( "プレイヤー " )
	squad01 = game->CreateFriendSquad( false, 1.000000, 4, "app:/Object/AiArmySoldier_AF.sgo", "app:/Object/AiArmySoldier_AF_Leader.sgo", 0.500000, "小隊01" )
	game->SetRouteMovementSpeed( 0.700000, squad01 )
	game->SetAiRoute( "小隊01ルート1", squad01 )
	game->CreateEnemyGroup( false, 1.000000, 15, "app:/Object/GiantAnt01B.sgo", 30.000000, "蟻01" )
	game->CreateEnemyGroup( false, 1.000000, 15, "app:/Object/GiantAnt01.sgo", 30.000000, "蟻01" )
	game->2C(900, 1.000000, "app:/object/man01.sgo" )
	game->2C(900, 1.000000, "app:/object/man02.sgo" )
	game->2C(900, 1.000000, "app:/object/man03.sgo" )
	game->2C(900, 1.000000, "app:/object/woman01.sgo" )
	game->2C(900, 1.000000, "app:/object/woman02.sgo" )
	game->CreateMob_Path( 60.000000, 0.100000, 10, "群衆01ルート1" )
	game->CreateMob_Path( 80.000000, 0.100000, 10, "群衆02ルート1" )
	game->PlayBgm( "BGM_E4M03_Hoshoku" )
	game->RegisterEvent( 0.000000, "Func1773602826", STACK_SIZE_ERROR )
	game->Factor_AllEnemyDestroy( 1.000000 )
	game->RegisterEvent( 0.000000, "Func1509116682", STACK_SIZE_ERROR )
	game->Factor_Wait( 1.000000 )
	relIndex += 1
}
Func1023809796(  )
{
	relIndex -= 1
	Func1023809796___Counter = increment( Func1023809796___Counter )
	if( Func1023809796___Counter != true )
	{
	}
	game->2C(910, true )
	game->CreateEnemyGroup( false, 1.000000, 10, "app:/Object/GiantAnt01B.sgo", 45.000000, "蟻03" )
	game->CreateEnemyGroup( false, 1.000000, 10, "app:/Object/GiantAnt01.sgo", 45.000000, "蟻03" )
	game->CreateEnemyGroup( false, 1.000000, 10, "app:/Object/GiantAnt01B.sgo", 45.000000, "蟻03_2" )
	game->CreateEnemyGroup( false, 1.000000, 10, "app:/Object/GiantAnt01.sgo", 45.000000, "蟻03_2" )
	game->CreateMob_Path( 75.000000, 0.100000, 10, "群衆05ルート1" )
	game->CreateMob_Path( 90.000000, 0.500000, 10, "群衆06ルート1" )
	game->RegisterEvent( 0.000000, "Func1937638982", STACK_SIZE_ERROR )
	game->Factor_AllEnemyDestroy( 1.000000 )
	game->RegisterEvent( 0.000000, "Func1397356889", STACK_SIZE_ERROR )
	game->Factor_Wait( 4.000000 )
	game->RegisterEvent( 0.000000, "Func828683967", STACK_SIZE_ERROR )
	relIndex += 1
}
Func1937638982(  )
{
	relIndex -= 1
	Func1937638982___Counter = increment( Func1937638982___Counter )
	if( Func1937638982___Counter != true )
	{
	}
	game->2C(910, true )
	game->CreateEnemyGroup( false, 1.000000, 5, "app:/Object/GiantAnt01B.sgo", 40.000000, "蟻04" )
	game->CreateEnemyGroup( false, 1.000000, 20, "app:/Object/GiantAnt01.sgo", 40.000000, "蟻04" )
	game->CreateEnemyGroup( false, 1.000000, 5, "app:/Object/GiantAnt01B.sgo", 50.000000, "蟻05" )
	game->CreateEnemyGroup( false, 1.000000, 20, "app:/Object/GiantAnt01.sgo", 50.000000, "蟻05" )
	game->CreateEnemyGroup( false, 1.000000, 2, "app:/Object/GiantAnt01.sgo", 5.000000, "蟻06" )
	game->CreateMob_Path( 85.000000, 0.100000, 10, "群衆07ルート1" )
	game->CreateMob_Path( 95.000000, 2.000000, 10, "群衆08ルート1" )
	game->RegisterEvent( 0.000000, "Func2114950596", STACK_SIZE_ERROR )
	game->Factor_AllEnemyDestroy( 0.100000 )
	game->RegisterEvent( 0.000000, "Func1771194658", STACK_SIZE_ERROR )
	game->RegisterEvent( 0.000000, "Func1345264074", STACK_SIZE_ERROR )
	relIndex += 1
}
Func2114950596(  )
{
	relIndex -= 1
	Func2114950596___Counter = increment( Func2114950596___Counter )
	if( Func2114950596___Counter != true )
	{
	}
	RadioBegin(  )
	game->2C(4000, "E007_18_E", true )
	game->Function_0xA2(  )
	game->Function_0xc8( 0.500000 )
	game->2C(4000, "E007_20_E" )
	game->Function_0xA2(  )
	game->Function_0xc8( 0.600000 )
	game->2C(4000, "E007_21_E" )
	RadioEnd(  )
	game->Function_0xA2(  )
	RadioVoice( 2.000000, "E007_23_E" )
	MissionClear(  )
	relIndex += 1
}
Func1773602826(  )
{
	relIndex -= 1
	Func1773602826___Counter = increment( Func1773602826___Counter )
	if( Func1773602826___Counter != true )
	{
	}
	game->2C(910, true )
	game->CreateEnemyGroup( false, 1.000000, 14, "app:/Object/GiantAnt01.sgo", 38.000000, "蟻02" )
	game->CreateEnemyGroup( false, 1.000000, 10, "app:/Object/GiantAnt01B.sgo", 38.000000, "蟻02" )
	game->CreateEnemyGroup( false, 1.000000, 6, "app:/Object/GiantAnt01.sgo", 38.000000, "蟻02_2" )
	game->CreateEnemyGroup( false, 1.000000, 5, "app:/Object/GiantAnt01B.sgo", 38.000000, "蟻02_2" )
	game->CreateMob_Path( 70.000000, 0.100000, 9, "群衆03ルート1" )
	game->CreateMob_Path( 90.000000, 1.500000, 11, "群衆04ルート1" )
	game->RegisterEvent( 0.000000, "Func1023809796", STACK_SIZE_ERROR )
	game->Factor_AllEnemyDestroy( 1.000000 )
	game->RegisterEvent( 0.000000, "Func966827433", STACK_SIZE_ERROR )
	game->Factor_Wait( 5.000000 )
	game->RegisterEvent( 0.000000, "Func781102749", STACK_SIZE_ERROR )
	game->Factor_AiMoveEnd( squad01 )
	relIndex += 1
}
Func601545874(  )
{
	relIndex -= 1
	Func601545874___Counter = increment( Func601545874___Counter )
	if( Func601545874___Counter != true )
	{
	}
	RadioVoice( 0.000000, "E007_16_E" )
	relIndex += 1
}
Func204131153(  )
{
	relIndex -= 1
	Func204131153___Counter = increment( Func204131153___Counter )
	if( Func204131153___Counter != true )
	{
	}
	RadioVoice( 0.000000, "E007_15_E" )
	game->RegisterEvent( 0.000000, "Func601545874", true )
	game->Factor_Wait( 30.000000 )
	relIndex += 1
}
Func1013448594(  )
{
	relIndex -= 1
	Func1013448594___Counter = increment( Func1013448594___Counter )
	if( Func1013448594___Counter != true )
	{
	}
	RadioVoice( 0.000000, "E007_14_E" )
	game->RegisterEvent( 0.000000, "Func809623698", true )
	game->Factor_Wait( 35.000000 )
	relIndex += 1
}
Func1771194658(  )
{
	relIndex -= 1
	Func1771194658___Counter = increment( Func1771194658___Counter )
	if( Func1771194658___Counter != true )
	{
	}
	wing01 = game->CreateFriendSquad( false, 1.000000, 5, "app:/Object/AiPaleWing_Lance.sgo", "app:/Object/AiPaleWing_Lance_Leader.sgo", 0.500000, "新ウイング " )
	game->SetAiRoute( "新ウイングルート ", wing01 )
	game->SetRouteMovementSpeed( 0.700000, wing01 )
	game->RegisterEvent( 0.000000, "Func965809876", true )
	relIndex += 1
}
Func965809876(  )
{
	relIndex -= 1
	Func965809876___Counter = increment( Func965809876___Counter )
	if( Func965809876___Counter != true )
	{
	}
	game->2C(302, 3.000000, "BGM_E4M05_Totsugeki", true )
	RadioVoice( 0.000000, "E007_01_E" )
	RadioVoice( 1.200000, "E007_02_E" )
	RadioVoice( 0.700000, "E007_03_E" )
	RadioVoice( 1.200000, "E007_06_E" )
	game->RegisterEvent( 0.000000, "Func1322596735", STACK_SIZE_ERROR )
	game->Factor_Wait( 5.000000 )
	relIndex += 1
}
Func1322596735(  )
{
	relIndex -= 1
	Func1322596735___Counter = increment( Func1322596735___Counter )
	if( Func1322596735___Counter != true )
	{
	}
	RadioVoice( 0.000000, "E007_08_E" )
	RadioVoice( 1.200000, "E007_09_E" )
	RadioVoice( 1.200000, "E007_10_E" )
	RadioVoice( 1.200000, "E007_11_E" )
	game->RegisterEvent( 0.000000, "Func1134176992", true )
	game->Factor_Wait( 15.000000 )
	relIndex += 1
}
Func1134176992(  )
{
	relIndex -= 1
	Func1134176992___Counter = increment( Func1134176992___Counter )
	if( Func1134176992___Counter != true )
	{
	}
	RadioVoice( 0.700000, "E007_07_E" )
	game->RegisterEvent( 0.000000, "Func1013448594", true )
	game->Factor_Wait( 8.000000 )
	relIndex += 1
}
Func966827433(  )
{
	relIndex -= 1
	Func966827433___Counter = increment( Func966827433___Counter )
	if( Func966827433___Counter != true )
	{
	}
	RadioVoice( 0.000000, "E005_17_E" )
	RadioVoice( 1.000000, "E005_18_E" )
	game->RegisterEvent( 0.000000, "Func1015679300", true )
	game->Factor_Wait( 4.000000 )
	relIndex += 1
}
Func1397356889(  )
{
	relIndex -= 1
	Func1397356889___Counter = increment( Func1397356889___Counter )
	if( Func1397356889___Counter != true )
	{
	}
	RadioVoice( 0.000000, "E006_03_E" )
	game->RegisterEvent( 0.000000, "Func24149959", true )
	game->Factor_Wait( 5.000000 )
	relIndex += 1
}
Func24149959(  )
{
	relIndex -= 1
	Func24149959___Counter = increment( Func24149959___Counter )
	if( Func24149959___Counter != true )
	{
	}
	RadioVoice( 0.000000, "E006_04_E" )
	game->RegisterEvent( 0.000000, "Func393958835", true )
	game->Factor_Wait( 5.000000 )
	relIndex += 1
}
Func1509116682(  )
{
	relIndex -= 1
	Func1509116682___Counter = increment( Func1509116682___Counter )
	if( Func1509116682___Counter != true )
	{
	}
	RadioVoice( 0.000000, "E006_01_E" )
	relIndex += 1
}
Func1015679300(  )
{
	relIndex -= 1
	Func1015679300___Counter = increment( Func1015679300___Counter )
	if( Func1015679300___Counter != true )
	{
	}
	RadioVoice( 0.000000, "E005_19_E" )
	RadioVoice( 1.000000, "E005_20_E" )
	relIndex += 1
}
Func393958835(  )
{
	relIndex -= 1
	Func393958835___Counter = increment( Func393958835___Counter )
	if( Func393958835___Counter != true )
	{
	}
	RadioVoice( 0.000000, "E006_05_E" )
	RadioVoice( 1.000000, "E006_06_E" )
	relIndex += 1
}
Func781102749(  )
{
	relIndex -= 1
	Func781102749___Counter = increment( Func781102749___Counter )
	if( Func781102749___Counter != true )
	{
	}
	game->SetAiRoute( "小隊01ルート2", squad01 )
	game->RegisterEvent( 0.000000, "Func828683967", true )
	game->Factor_AiMoveEnd( squad01 )
	relIndex += 1
}
Func828683967(  )
{
	relIndex -= 1
	Func828683967___Counter = increment( Func828683967___Counter )
	if( Func828683967___Counter != 2 )
	{
	}
	game->SetAiRoute( "小隊01ルート3", squad01 )
	game->RegisterEvent( 0.000000, "Func1345264074", true )
	game->Factor_AiMoveEnd( squad01 )
	relIndex += 1
}
Func1345264074(  )
{
	relIndex -= 1
	Func1345264074___Counter = increment( Func1345264074___Counter )
	if( Func1345264074___Counter != 2 )
	{
	}
	game->SetAiRoute( "小隊01ルート4", squad01 )
	game->SetRouteMovementSpeed( 0.400000, squad01 )
	relIndex += 1
}
Func809623698(  )
{
	relIndex -= 1
	Func809623698___Counter = increment( Func809623698___Counter )
	if( Func809623698___Counter != true )
	{
	}
	RadioVoice( 0.000000, "E007_13_E" )
	game->RegisterEvent( 0.000000, "Func204131153", true )
	game->Factor_Wait( 10.000000 )
}
