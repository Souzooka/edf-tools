#include "stdafx.h"

#include <iostream>
#include <fstream>
#include <Windows.h>
#include <string>
#include <vector>
#include <locale>
#include <codecvt>

#include <iostream>
#include <locale>
#include <locale.h>
#include "util.h"
#include "RMPA.h"

#define TYPE_HEADER_SIZE 0x20

int CRMPA::Read( const std::wstring& path )
{
	std::ifstream file( path + L".RMPA", std::ios::binary | std::ios::ate);

	std::streamsize size = file.tellg();
	file.seekg(0, std::ios::beg);

	std::vector<char> buffer(size);
	if( file.read(buffer.data(), size) )
	{
		unsigned char seg[4];

		int position = 0;
		//position = 0x100;

		Read4BytesReversed( seg, buffer, position );

		bool validHeader = false;
		if( seg[0] == 0x00 && seg[1] == 0x50 && seg[2] == 0x4D && seg[3] == 0x52 )
			validHeader = true;

		if( !validHeader )
		{
			std::wcout << L"BAD FILE\n";
			file.close();
			return -1;
		}

		//std::wofstream output("RMPA_Data.txt", std::ios::binary | std::ios::out | std::ios::ate );

		//const unsigned long MaxCode = 0x10ffff;
		//const std::codecvt_mode Mode = std::generate_header;
		//std::locale utf16_locale(output.getloc(), new std::codecvt_utf16<wchar_t, MaxCode, Mode>);
		//output.imbue(utf16_locale);

		//Parse the header
		bool hasRoutes = false;
		bool hasShapes = false;
		bool hasCamData = false;
		bool hasSpawnPoints = false;

		//0x8: Bool denoting if RMPA has route data
		position = 0x8;
		Read4BytesReversed( seg, buffer, position );
		hasRoutes = GetIntFromChunk( seg );
		std::wcout << L"Has Routes: " + ToString( hasRoutes ) + L"\n";

		position = 0xc;
		Read4BytesReversed( seg, buffer, position );
		int routePos = GetIntFromChunk( seg );

		//0x10: Bool denoting if RMPA has shape data
		position = 0x10;
		Read4BytesReversed( seg, buffer, position );
		hasShapes = GetIntFromChunk( seg );
		std::wcout << L"Has Shapes: " + ToString( hasShapes ) + L"\n";

		position = 0x14;
		Read4BytesReversed( seg, buffer, position );
		int shapePos = GetIntFromChunk( seg );

		//0x18: Bool denoting if RMPA has camera data
		position = 0x18;
		Read4BytesReversed( seg, buffer, position );
		hasCamData = GetIntFromChunk( seg );
		std::wcout << L"Has Camera Data: " + ToString( hasCamData ) + L"\n";

		position = 0x1C;
		Read4BytesReversed( seg, buffer, position );
		int camPos = GetIntFromChunk( seg );

		//0x18: Bool denoting if RMPA has spawnpoint data
		position = 0x20;
		Read4BytesReversed( seg, buffer, position );
		hasSpawnPoints = GetIntFromChunk( seg );
		std::wcout << L"Has Spawnpoint Data: " + ToString( hasSpawnPoints ) + L"\n";

		position = 0x24;
		Read4BytesReversed( seg, buffer, position );
		int spawnPos = GetIntFromChunk( seg );

		//Read spawnpoints:
		position = spawnPos;
		Read4BytesReversed( seg, buffer, position );
		int numSubheaders = GetIntFromChunk( seg );
		std::wcout << L"Enumerators: " + ToString( numSubheaders ) + L"\n";

		position += 0x4;
		Read4BytesReversed( seg, buffer, position );
		int ofs = GetIntFromChunk( seg );
		std::wcout << L"Enumerators 1 pos: " + ToString( ofs ) + L"\n";

		//Subheader
		for( int i = 0; i < numSubheaders; i++ )
		{
			std::wcout << L"Enumerator " + ToString( i ) + L":\n";

			position = spawnPos + ofs + 0x8;
			Read4BytesReversed( seg, buffer, position );
			int endOfs = GetIntFromChunk( seg );
			std::wcout << L"Enumerator ends at: " + ToString( endOfs ) + L"\n";

			position = spawnPos + ofs + 0x18;
			Read4BytesReversed( seg, buffer, position );
			int dataCount = GetIntFromChunk( seg );
			std::wcout << L"Number of data in Enumerator " + ToString( i ) + L": " + ToString( dataCount ) + L"\n";

			position = spawnPos + ofs + 0x1C;
			Read4BytesReversed( seg, buffer, position );
			int num = GetIntFromChunk( seg );
			std::wcout << L"Data should begin at: " + ToString( num ) + L"\n";

			int clusterPos = spawnPos + ofs + num;

			spawnPoints.push_back( ReadSpawnpoint( clusterPos, buffer ) );

			//Read nodes
			for( int j = 0; j < dataCount; j++ )
			{
				//0x8 data
				position = clusterPos + 0x8;
				Read4BytesReversed( seg, buffer, position );
				int num = GetIntFromChunk( seg );
				std::wcout << ToString( num ) + L" Name: ";

				//Node name:
				position = clusterPos + 0x34;
				Read4BytesReversed( seg, buffer, position );
				num = GetIntFromChunk( seg );
				//num++;
				std::wcout << ReadUnicode( buffer, clusterPos + num, true );

				position = clusterPos;
				position += 0x8;

				std::wcout << L"Position: ";
				for( int i = 0; i < 3; i++ )
				{
					position += 4;
					Read4Bytes( seg, buffer, position );

					float f;
					memcpy( &f, &seg, sizeof( f ) );
					std::wcout << ToString( f ) + L" ";
				}

				std::wcout << L"Rotation: ";
				for( int i = 0; i < 3; i++ )
				{
					position += 4;
					Read4Bytes( seg, buffer, position );

					float f;
					memcpy( &f, &seg, sizeof( f ) );
					std::wcout << ToString( f ) + L" ";
				}
				std::wcout << L"\n";
				clusterPos += 0x40;
			}

			ofs += 0x20;
		}

		//Lets do soemthing with our data:
		std::wstring input;
		std::wcin >> input;


#if 0
		int clusterPos;


		//Waypoints:
		clusterPos = position = 0x90;

		for( int i = 0; i < 14; i++ )
		{
			position = clusterPos;

			position = clusterPos + 0x10;
			Read4BytesReversed( seg, buffer, position );
			int sgoOffs = GetIntFromChunk( seg );
			position = clusterPos + 0x18;
			Read4BytesReversed( seg, buffer, position );
			int sgoSize = GetIntFromChunk( seg );

			position = clusterPos + sgoOffs;
			//Pull out the SGO:
			std::vector< unsigned char > sgoBuf;
			for( int j = 0; j > sgoSize; j++ )
			{
				sgoBuf.push_back( buffer[j] );
			}

			position = clusterPos + 0x14;

			Read4BytesReversed( seg, buffer, position );
			int num = GetIntFromChunk( seg );
			std::wcout << ToString( num );

			position = clusterPos;

			Read4BytesReversed( seg, buffer, position );
			num = GetIntFromChunk( seg );
			std::wcout << L": Waypoint Number: " + ToString( num ) + L" Name:";

			//Node name:
			position = clusterPos + 0x24;
			Read4BytesReversed( seg, buffer, position );
			num = GetIntFromChunk( seg );
			std::wcout << ReadUnicode( buffer, clusterPos + num );
			output << ReadUnicode( buffer, clusterPos + num ); 

			position = clusterPos + 0x21;

			std::wcout << "Position: ";
			for( int i = 0; i < 3; i++ )
			{
				position += 4;
				Read4BytesReversed( seg, buffer, position );

				float f;
				memcpy(&f, &seg, sizeof(f));
				std::wcout << ToString( f ) + L" ";
			}

			std::wcout << "\r\n";

			clusterPos += 0x3c;
		}

		Read4BytesReversed( seg, buffer, 0x14 );
		position = GetIntFromChunk( seg );
		position += 0x5c;
		Read4BytesReversed( seg, buffer, position );
		position -= 0x5c;
		position += GetIntFromChunk( seg );

		clusterPos = position;

		//Spawnpoints:
		//clusterPos = position = 0xFD0;
		for( int i = 0; i < 8; i++ )
		{
			//0x8 data
			position = clusterPos + 0x8;
			Read4BytesReversed( seg, buffer, position );
			int num = GetIntFromChunk( seg );
			std::wcout << ToString( num ) + L" Name: ";

			//Node name:
			position = clusterPos + 0x34;
			Read4BytesReversed( seg, buffer, position );
			num = GetIntFromChunk( seg );
			num++;
			std::wcout << ReadUnicode( buffer, clusterPos + num );
			output << ReadUnicode( buffer, clusterPos + num ); 

			position = clusterPos;
			position += 0x8;

			std::wcout << "Position: ";
			for( int i = 0; i < 3; i++ )
			{
				position += 4;
				Read4Bytes( seg, buffer, position );

				float f;
				memcpy(&f, &seg, sizeof(f));
				std::wcout << ToString( f ) + L" ";
			}

			std::wcout << "Rotation: ";
			for( int i = 0; i < 3; i++ )
			{
				position += 4;
				Read4Bytes( seg, buffer, position );

				float f;
				memcpy(&f, &seg, sizeof(f));
				std::wcout << ToString( f ) + L" ";
			}
			std::wcout << "\r\n";
			clusterPos += 0x40;
		}
		#endif
	}
}

RMPASpawnPoint CRMPA::ReadSpawnpoint( int pos, std::vector<char> buffer )
{
	unsigned char seg[] = { 0x0, 0x0, 0x0, 0x0 };

	//0x8 data
	int position = pos + 0x8;
	Read4BytesReversed( seg, buffer, position );
	int num = GetIntFromChunk( seg );

	//Node name:
	position = pos + 0x34;
	Read4BytesReversed( seg, buffer, position );
	num = GetIntFromChunk( seg );
	//num++;
	std::wstring nodeName = ReadUnicode( buffer, pos + num, true );

	position = pos;
	position += 0x8;

	float vecPos[3];
	for( int i = 0; i < 3; i++ )
	{
		position += 4;
		Read4Bytes( seg, buffer, position );

		float f;
		memcpy( &f, &seg, sizeof( f ) );
		vecPos[i] = f;
	}

	float vecAng[3];
	for( int i = 0; i < 3; i++ )
	{
		position += 4;
		Read4Bytes( seg, buffer, position );

		float f;
		memcpy( &f, &seg, sizeof( f ) );
		vecAng[i] = f;
	}
	
	return RMPASpawnPoint( nodeName, vecPos[0], vecPos[1], vecPos[2], vecAng[0], vecAng[1], vecAng[2] );
}