#include "stdafx.h"
#include <string>
#include <vector>
#include "util.h"

void Read4Bytes( unsigned char *chunk, std::vector<char> buf, int pos )
{
	/*if( globals->endianMode )
	{
		Read4BytesReversed( chunk, buf, pos );
		return;
	}*/

	chunk[3] = buf[ pos ];
	chunk[2] = buf[ pos + 1 ];
	chunk[1] = buf[ pos + 2 ];
	chunk[0] = buf[ pos + 3 ];
}

void Read4BytesReversed( unsigned char *chunk, std::vector<char> buf, int pos )
{
	chunk[0] = buf[ pos ];
	chunk[1] = buf[ pos + 1 ];
	chunk[2] = buf[ pos + 2 ];
	chunk[3] = buf[ pos + 3 ];
}

int GetIntFromChunk( unsigned char *chunk )
{
	int num = 0;
	for(int i = 0; i < 4; i++)
	{
		num <<= 8;
		num |= chunk[i];
	}

	return num;
}

std::wstring ReadUnicode( std::vector<char> chunk, int pos, bool swapEndian )
{
	if( pos > chunk.size( ) )
		return L"";

	unsigned int bufPos = pos;

	std::vector< unsigned char > bytes;

	int zeroCount = 0;

	//Repeat until EOF, or otherwise broken
	while( bufPos < chunk.size() )
	{
		if( swapEndian )
		{
			if( bufPos % 2 )
			{
				bytes.push_back( chunk[bufPos] );
				bytes.push_back( chunk[bufPos-1] );
			}
		}
		else
			bytes.push_back( chunk[bufPos] );

		if( chunk[bufPos] == 0x00 )
		{
			zeroCount++;
			if( zeroCount == 2 )
				break;
		}
		else
			zeroCount = 0;
		bufPos++;
	}

	//if( bytes.size( ) == 0 );
	//return L"";


	std::wstring wstr(reinterpret_cast<wchar_t*>(&bytes[0]), bytes.size()/sizeof(wchar_t));

	return wstr;
}

std::wstring ToString( int i )
{
	return std::to_wstring( (uint64_t) i );
}
std::wstring ToString( float f )
{
	return std::to_wstring( (long double) f );
}