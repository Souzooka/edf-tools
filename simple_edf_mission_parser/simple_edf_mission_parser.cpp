// simple_edf_mission_parser.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <Windows.h>
#include <string>
#include <vector>
#include <locale>
#include <codecvt>

#include <iostream>
#include <locale>
#include <locale.h>

#ifndef MS_STDLIB_BUGS
#  if ( _MSC_VER || __MINGW32__ || __MSVCRT__ )
#    define MS_STDLIB_BUGS 1
#  else
#    define MS_STDLIB_BUGS 0
#  endif
#endif

#if MS_STDLIB_BUGS
#  include <io.h>
#  include <fcntl.h>
#endif

void init_locale(void)
{
#if MS_STDLIB_BUGS
  char cp_utf16le[] = ".1200";
  setlocale( LC_ALL, cp_utf16le );
  _setmode( _fileno(stdout), _O_WTEXT );
#else
  // The correct locale name may vary by OS, e.g., "en_US.utf8".
  constexpr char locale_name[] = "";
  setlocale( LC_ALL, locale_name );
  std::locale::global(std::locale(locale_name));
  std::wcin.imbue(std::locale())
  std::wcout.imbue(std::locale());
#endif
}


#include "util.h"
#include "MissionScript.h" //TODO: Implement mission script class that stores and proccess data
#include "RMPA.h" //TODO: Implement RMPA class that stores and proccess data

void ProcessFile( const std::wstring& path, int extraFlags )
{
    using namespace std;

	//Get file extension:
	size_t lastindex = path.find_last_of( L"." );

	if( lastindex != wstring::npos )
	{ 
		wstring extension = path.substr( lastindex + 1, extension.size( ) - lastindex );
		wstring strn = path.substr( 0, lastindex );

		if( extension == L"rmpa" )
		{
            unique_ptr<CRMPA> rmpa = make_unique<CRMPA>();
			rmpa->Read( strn );
		}
		else if( extension == L"txt" )
		{
            unique_ptr<CMissionScript> script = make_unique<CMissionScript>();
			script->Write( strn, extraFlags );
		}
		else if( extension == L"bvm" )
		{
            unique_ptr<CMissionScript> script = make_unique<CMissionScript>();
			script->Read( strn );
		}
	}
	else
	{
		//Search for valid files:


	}
}

int _tmain( int argc, wchar_t* argv[] )
{
    using namespace std;

	init_locale( );
	wstring path;

	if( argc > 1 )
	{
		wcout << L"Parsing file: " << argv[1] << L'\n';

		ProcessFile( std::wstring(argv[1]), 1 );
		/*
		std::wcout << L"Compile (0) or decompile (1)?: ";
		std::wcin >> path;

		if( stoi( path ) == 0 )
		{
			CMissionScript *script = new CMissionScript( );
			std::wstring strn = argv[1];
			size_t lastindex = strn.find_last_of( L"." );
			strn = strn.substr( 0, lastindex );

			//lastindex = strn.find_last_of( L"\\" );
			//strn = strn.substr( lastindex + 1, strn.size() - lastindex );

			script->Write( strn, 1 );
			delete script;
		}
		if( stoi( path ) == 1 )
		{
			std::wstring strn = argv[1];
			size_t lastindex = strn.find_last_of( L"." );
			strn = strn.substr( 0, lastindex );

			CMissionScript *script = new CMissionScript( );
			script->Read( strn );
			delete script;
			std::wcout << "\n";
		}
		*/
	}
	else
	{
		wcout << L"Filename:";
		wcin >> path;
		wcout << L"\n";

		wcout << L"Parsing file...\n";

		ProcessFile( path, 1 );

		//CMissionScript *script = new CMissionScript( );
		//script->Read( path );

		//script->Write( L"testMission", 1 );
		//delete script;

		//script = new CMissionScript( );
		//script->Read( L"testMission" );

		//delete script;

		//CRMPA rmpa;
		//rmpa.Read( path );

		wcout << "\n";
	}
	system( "pause" );
	
	return 0;
}

